#include "Shape.h"

Shape::Shape()
{
}

/*
Shape::Shape(const string& type, const string& name)
{
	this->_name = name;
	this->_type = type;
}*/

void Shape::printDetails() const
{
	std::cout << this->getType().c_str() << " " << this->getName().c_str() << " " << this->getArea() << " " << this->getPerimeter() << std::endl;
}

string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}
