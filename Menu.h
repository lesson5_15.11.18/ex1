#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();

	void CreateShape(int option);//Creating a shaped based on the choice: 1-circle, ....
	void printMenu();//main menu
	void printShapesMenu();//option 0 menu
	void printInfoMenu();//option 1 menu
	void printSubInfoMenu();//menu for any shape from option 1
	void clearAll();//Deleting all shapes
private: 

	//Drawing functions, calling from switch case
	void ArrowCase();
	void CircleCase();
	void TriCase();
	void RectCase();
	
	void redrawExclusive(int num);//redraws the board without the deleted shape
	void printMoveMenu(int num);//To move the shape

	vector<Shape*> _shapes;

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

