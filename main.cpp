#include "Menu.h"

void shapeChooser(Menu& m)
{
	int option = 0;

	m.printShapesMenu();
	std::cin >> option;
	m.CreateShape(option);
}

void main()
{
	Menu* m = new Menu();//Creating Menu object
	int option = 0;
	while (option != 3)//main loop till the user wants to exit
	{
		m->printMenu();//print main menu
		std::cin >> option;//getting choice
		switch (option)
		{
		case 0://Choice 0 which is to create a new shape
			shapeChooser(*m);
			break;
		case 1://Choice 1 which is info about shapes
			m->printInfoMenu();
			break;
		case 2://Choice 2 which is to clear the display
			m->clearAll();
			break;
		default:
			break;
		}
	}

	getchar();
}
