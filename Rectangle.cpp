#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point & a, double length, double width, const string & type, const string & name)
{
	if(length > 0 && width > 0)
	{
		this->_leftCorner = *new Point(a);
		this->_length = length;
		this->_width = width;
		this->_name = name;
		this->_type = type;
		this->_points.push_back(*new Point(this->_leftCorner));//left top corner
		this->_points.push_back(*new Point(this->_leftCorner.getX() + this->_width, this->_leftCorner.getY() + this->_length));//right bottom corner
	}
	else
	{
		std::cout << "Length or width can't be 0";
	}
}

myShapes::Rectangle::~Rectangle()
{
	delete &this->_leftCorner;
}

double myShapes::Rectangle::getArea() const
{
	return this->_width + this->_length;
}

double myShapes::Rectangle::getPerimeter() const
{
	return 2 * (this->_length + this->_width);
}

void myShapes::Rectangle::move(const Point & other)
{
	this->_leftCorner += other;
}

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}
