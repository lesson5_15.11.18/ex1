#include "Menu.h"


Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

/*
	Create shape by chocie, 0 for circle, 1 for arrow....
*/
void Menu::CreateShape(int option)
{
	
	switch (option)
	{
	case 0://Create Circle
		this->CircleCase();
		break;
	case 1://Create Arrow
		this->ArrowCase();
		break;
	case 2://Create Triangle
		TriCase();
		break;
	case 3://Create Rectangle
		RectCase();
		break;
	default:
		break;
	}
	system("PAUSE");
}

/*
	Creates,displays the arrow on the board and saves the arrow to the _shapes vector
*/
void Menu::ArrowCase()
{
	std::string name = "";
	std::string type = "";
	
	Arrow* a;//New arrow Object, pointer so it saved dynamicly

	Point* p[2];//Arrow got two points

	//loop vars
	int i = 0;
	int x = 0;
	int y = 0;
	
	type = "Arrow";//type is Arrow

	//gets the X and Y for each point of the arrow, saves in the p array
	for (i = 0; i < 2; i++)
	{
		std::cout << "Enter the X of point number: " << i+1 << std::endl;
		std::cin >> x;

		std::cout << "Enter the Y of point number: " << i+1 << std::endl;
		std::cin >> y;

		p[i] = new Point(x, y);// creates new dynamic object of Point
	}

	//Gets name
	std::cout << "Enter the name of the shape:";
	std::cin >> name;
	
	a = new Arrow(*p[0], *p[1], type, name);//Creates the arrow dynamicly so the object wont get deleted when out of the function
	a->draw(*this->_disp, *this->_board);//Draws the arrow
	this->_shapes.push_back(a);//Adds the shape's object to the _shape vector to save all the data about it
}

/*
Creates,displays the cicle on the board and saves the arrow to the _shapes vector
*/
void Menu::CircleCase()
{
	std::string name = "";
	std::string type = "";
	
	Circle* c;

	int p1[2] = { 0 };
	double rad = 0;
	
	type = "Circle";
	std::cout << "Please enter X" << std::endl;
	std::cin >> p1[0];

	std::cout << "Please enter Y" << std::endl;
	std::cin >> p1[1];


	std::cout << "Please enter radius" << std::endl;
	std::cin >> rad;

	std::cout << "Enter the name of the shape:";
	std::cin >> name;

	c = new Circle(*new Point(p1[0], p1[1]), rad, type, name);
	c->draw(*this->_disp, *this->_board);

	this->_shapes.push_back(c);
}
/*
	Creates,displays the triangle on the board and saves the arrow to the _shapes vector
*/
void Menu::TriCase()
{
	std::string name = "";
	std::string type = "";

	Triangle* t;

	Point* p[3];

	int i = 0;
	int x = 0;
	int y = 0;
	
	type = "Triangle";

	for (i = 0; i < 3; i++)
	{
		std::cout << "Enter the X of point number: " << i+1 << std::endl;
		std::cin >> x;

		std::cout << "Enter the Y of point number: " << i+1 << std::endl;
		std::cin >> y;

		p[i] = new Point(x, y);
	}

	std::cout << "Enter the name of the shape:";
	std::cin >> name;

	t = new Triangle(*p[0], *p[1], *p[2], type, name);
	if (t->getType() == "Triangle")//If the triangle isn't a line so the type is "Triangle"
	{
		t->draw(*this->_disp, *this->_board);
		this->_shapes.push_back(t);
	}
}
/*
	Creates,displays the rectangle on the board and saves the arrow to the _shapes vector
*/
void Menu::RectCase()
{
	std::string name = "";
	std::string type = "";

	myShapes::Rectangle* r;

	int i = 0;
	int x = 0;
	int y = 0;

	int length = 0;
	int width = 0;

	type = "Rectangle";

	std::cout << "Enter the X of the top left corner: " << std::endl;
	std::cin >> x;

	std::cout << "Enter the Y of the top left corner: " << std::endl;
	std::cin >> y;

	std::cout << "Please enter the length of the shape:" << std::endl;
	std::cin >> length;

	std::cout << "Please enter the width of the shape:" << std::endl;
	std::cin >> width;

	r = new myShapes::Rectangle(*new Point(x,y), length, width, type, name);
	r->draw(*this->_disp, *this->_board);
	this->_shapes.push_back(r);
}

/*
	gets a number of the shape, and clears it from the board, keeps all the other unchanged
*/
void Menu::redrawExclusive(int num)
{
	int i = 0;
	int size = this->_shapes.size();// size of the vector, save it because it changes while loop
	vector<Shape*> temp;//new _shape vector to hold the new vector without the deleted shape

	//clears all the shapes from the board
	for (i = size - 1; i >= 0; i--)
	{
		this->_shapes[i]->clearDraw(*this->_disp, *this->_board);
	}

	//redraws all the shaped which aren't the shape-to-be-deleted
	for (i = size - 1; i >= 0; i--)
	{
		if (this->_shapes[num] != this->_shapes[i])
		{
			this->_shapes[i]->draw(*this->_disp, *this->_board);
			temp.push_back(this->_shapes[i]);
		}
	}

	this->_shapes = temp;
}

/*
	Main menu print 
*/
void Menu::printMenu()
{
	system("cls");//Clear Screen
	std::cout << "Enter 0 to add a new shape." << std::endl;
	std::cout << "Enter 1 to modify or get information from a current shape." << std::endl;
	std::cout << "Enter 2 to delete all shapes." << std::endl;
	std::cout << "Enter 3 to exit." << std::endl;
}

/*
	New shape menu print
*/
void Menu::printShapesMenu()
{
	system("cls");//Clear Screen
	std::cout << "Enter 0 to add a circle." << std::endl;
	std::cout << "Enter 1 to add an arrow." << std::endl;
	std::cout << "Enter 2 to add a triangle." << std::endl;
	std::cout << "Enter 3 to add a rectangle." << std::endl;
}

/*
	Info and updates menu print
*/
void Menu::printInfoMenu()
{
	system("cls");
	int i = 0;
	int option = 0;
	int num = 0;
	for (i = 0; i < this->_shapes.size(); i++)
	{
		std::cout << "Enter " << i << " for " << this->_shapes[0]->getName() << "(" << this->_shapes[i]->getType() << ")" << std::endl;
	}

	if (this->_shapes.size() != 0)//if size = 0 there are no shapes so nothing to update or get info of
	{
		std::cin >> num;

		this->printSubInfoMenu();
		std::cin >> option;

		switch (option)
		{
		case 0:
			this->printMoveMenu(num);
			break;
		case 1:
			this->_shapes[num]->printDetails();
			break;
		case 2:
			this->redrawExclusive(num);
			break;
		default:
			break;
		}
		system("Pause");
	}
	
}

/*
	Prints the functions of the previus chocie
*/
void Menu::printSubInfoMenu()
{
	std::cout << "Enter 0 to move the shape" << std::endl;
	std::cout << "Enter 1 to get its details." << std::endl;
	std::cout << "Enter 2 to remove the shape." << std::endl;
}

/*
	Prints the move function and calls the shape.move
*/
void Menu::printMoveMenu(int num)
{
	int x = 0;
	int y = 0;

	system("cls");

	std::cout << "Please enter the X moving scale: " << std::endl;
	std::cin >> x;

	std::cout << "Please enter the Y moving scale: " << std::endl;
	std::cin >> y;

	this->_shapes[num]->clearDraw(*this->_disp, *this->_board);
	(*this->_shapes[num]).move(*new Point(x, y));
	this->_shapes[num]->draw(*this->_disp, *this->_board);
}

/*
	Clears all the shapes from the board
*/
void Menu::clearAll()
{
	int i = 0;
	int size = this->_shapes.size();

	for (i = size - 1; i >= 0; i--)
	{
		this->_shapes[i]->clearDraw(*this->_disp, *this->_board);
		delete this->_shapes[i];//clears the memory
		this->_shapes.pop_back();//Deleting the shape object from the vector
	}
}

