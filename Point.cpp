#include "Point.h"

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point & other)
{
	this->_x = other.getX();
	this->_y = other.getY();
}

Point::~Point()
{

}

Point Point::operator+(const Point & other) const
{
	Point* p = new Point(this->_x + other.getX(), this->_y + other.getY());
	return *p;
}

Point & Point::operator+=(const Point & other)
{
	this->_x += other._x;
	this->_y += other._y;
	return *this;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point & other) const
{
	double x = other.getX() - this->_x;
	double y = other.getY() - this->_y;

	double dis = sqrt(pow(x,2) + pow(y,2));

	return dis;
}
