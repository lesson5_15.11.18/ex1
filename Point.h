#pragma once
#include <vector>
#include <math.h>
//#include "CImg.h"

class Point
{
protected:
	double _x;
	double _y;

public:
	Point(double x, double y);
	Point(const Point& other = *new Point(0,0));
	virtual ~Point();
	
	Point operator+(const Point& other) const;
	Point& operator+=(const Point& other);

	double getX() const;
	double getY() const;

	double distance(const Point& other) const;

	
};