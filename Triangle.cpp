#include "Triangle.h"



Triangle::Triangle(const Point & a, const Point & b, const Point & c, const string & type, const string & name)
{
	if(a.getX() != b.getX() && b.getX() != c.getX() && a.getY() != b.getY() && b.getY() != c.getY())//Checking that the point are not a line
	{
		this->_points.push_back(*new Point(a));
		this->_points.push_back(*new Point(b));
		this->_points.push_back(*new Point(c));
		this->_name = name;
		this->_type = type;
	}
	else
	{
		std::cout << "The Points entered create a line" << std::endl;
	}
}

Triangle::~Triangle()
{
	delete[] &this->_points;
}

double Triangle::getArea() const
{
	return 0;
}

double Triangle::getPerimeter() const
{
	return this->_points[0].distance(this->_points[1]) + this->_points[1].distance(this->_points[2]) + this->_points[0].distance(this->_points[2]);
}

void Triangle::move(const Point & other)
{
	int i = 0;
	for (i = 0; i < this->_points.size(); i++)
	{
		this->_points[i] += other;
	}
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
