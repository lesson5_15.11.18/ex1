#pragma once

#include "Shape.h"
#include "Point.h"
#include <iostream>
#include <vector>
//#include "Cimg.h"

using namespace std;

class Polygon : public Shape
{
public:
	Polygon();
	//Polygon(const string& type = 0, const string& name = 0);
	virtual ~Polygon();

	// override functions if need (virtual + pure virtual)

protected:
	vector<Point> _points;
};